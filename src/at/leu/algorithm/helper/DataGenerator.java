package at.leu.algorithm.helper;


public class DataGenerator {
	
	public static int[] generateRandomData(int size, int minValue, int maxValue){
		
		int generated = 0;
		
		int[] randomData = new int[size];
		
		while (generated < size ) {
						
			randomData[generated]=(int) (Math.random()*(maxValue-minValue)+minValue);
			
			generated ++;
		}
	return randomData;
		
	}
	
	public static void printData(int[] data){
		int size = data.length;
		for(int i= 0; i < size; i++){
			System.out.println(data[i]);
		}
	}
	
	

}
