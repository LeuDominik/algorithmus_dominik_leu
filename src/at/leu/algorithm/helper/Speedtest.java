package at.leu.algorithm.helper;


import java.util.ArrayList;
import java.util.List;

import at.leu.algorithm.sortalgorithms.BubbleSorter;
import at.leu.algorithm.sortalgorithms.InsertionSorter;
import at.leu.algorithm.sortalgorithms.MySorter;
import at.leu.algorithm.sortalgorithms.SelectionSorter;
import at.leu.algorithm.sortalgorithms.SortAlgorithm;

public class Speedtest {
	private List<SortAlgorithm> algorithms = new ArrayList();

	public void add(SortAlgorithm algo){
		algorithms.add(algo);
	}
	
	public void run(){
		int[] data = DataGenerator.generateRandomData(500000, 0, 15987);
		System.out.println("fertig" );
		int[] clonedata = data.clone();
		for(SortAlgorithm sa : algorithms){
			data = clonedata.clone();
			sa.getName();
			long startTime = System.currentTimeMillis();
			sa.sort(data);
			long endTime = System.currentTimeMillis();
			long duration = (endTime - startTime);
			System.out.println(duration);
		}
	}
	
	
	public static void main(String[] args) {
		Speedtest speed = new Speedtest();
		
		BubbleSorter bs = new BubbleSorter();
		InsertionSorter is = new InsertionSorter();
		MySorter ms = new MySorter();
		SelectionSorter ss = new SelectionSorter();
		
		//speed.add(bs);
		speed.add(is);
		
		speed.add(ss);
		speed.add(ms);
		
		speed.run();
	}
}
