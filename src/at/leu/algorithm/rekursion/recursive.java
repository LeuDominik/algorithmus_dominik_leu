package at.leu.algorithm.rekursion;

public class recursive {
	
	public static void main(String[] args) {
		//System.out.println(getGgt(369,396));
		//System.out.println(getFactorial(2));
		//System.out.println(getPotenz(2,6));
		//printBinary(6);
		//printHex(698);
		//System.out.println(fibonacci(19));
		System.out.println(isPalindrom("annasusanna"));
		
	}

	
	public static int getGgt(int a, int b){
		System.out.println(a + ":" + b);
		if(a==b){
			return a;
		}else if(a>b){
			return getGgt(a-b, b);
		}else{
			return getGgt(a, b-a);
		}
		
	}
	
	public static int getFactorial(int x){
		System.out.println(x);
		if(x ==1){
			
			return 1;
		}else{
			return x * getFactorial(--x);
		}
	}

	
	public static int getPotenz(int a, int b){
		
		if(b==1){
			return a;
		}else{
			return a * getPotenz(a, --b);
		}
	}
	
	public static void printBinary(int input){
		if(input/2 ==0){
			System.out.print(input%2);
		}else{
			printBinary(input/2);
			System.out.print(input%2);
		}
	}
	
	public static void printHex(int input){
		
		if(input<16){
			System.out.print(Integer.toHexString(input));
		}else{
			printHex(input/16);
			System.out.print(Integer.toHexString(input%16));
		}
	}	
	
	public static int fibonacci(int position){
		if(position == 1 || position == 2){
			return 1;
		}else{
			return fibonacci(position-2) + fibonacci(--position);
		}
	}
	
	
	private static boolean isPalindrom(String str){
	    if(str.length() == 0 || str.length() == 1)
	            return true; 
	        if(str.charAt(0) == str.charAt(str.length()-1))
	            return isPalindrom(str.substring(1, str.length()-1));
	        else{
	          return false;
	        }
	}
}
