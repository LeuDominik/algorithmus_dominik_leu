package at.leu.algorithm.summe;

public class Aufgabe3 {
	public String OsternBerechnen(int Jahr){
		int N = Jahr-1900;
		int A = N %19;
		int B = (7*A)/19;
		int M = (11*A+4-B)%29;
		int Q = N /4;
		int W = (N+Q+31-M)%7;
		int P = 25 - M -W;
		String Ostern = "";
		if(P>0){
			Ostern ="Ostern ist am "+ P +"April.";
		}else{
			P = P+31;
			Ostern = "Ostern ist am "+ P +" M�rz.";
		}
		return Ostern;
	} 
	
	public static void main(String[] args) {
		Aufgabe3 ostern = new Aufgabe3();
		System.out.println(ostern.OsternBerechnen(2016));
	}

}
