package at.leu.algorithm.summe;

public class Aufgabe1 {
	
	public int Addieren (int zahl1, int zahl2){
		int ergebnis =0;
		int zehnerpotenz =1;

		char[] summand1 = (""+zahl1).toCharArray();
		char[] summand2 = (""+zahl2).toCharArray();
		
		int laenge =0;
		Boolean laenger1 = false;
		Boolean laenger2 = false;
		
		if(summand1.length< summand2.length){
			laenge = summand2.length-1;
			laenger2 = true;
		}else if (summand1.length> summand2.length){
			laenge =summand1.length-1;
			laenger1 = true;
		}else {laenge = summand1.length -1;}
		
		char [] groesser = new char [laenge+1];
		
		if (laenger1){
			for(int j = summand2.length-1; j>-1; j--){
				groesser [j] = summand2[j];
			}
		}else if (laenger2){
			for(int k =summand1.length-1; k>-1; k--){
				groesser [k] = summand1[k];
			}
		}
		
		for (int nullPruefung=laenge; nullPruefung > -1; nullPruefung --){
			System.out.println(groesser[nullPruefung]);
			/*if(){
				groesser[nullPruefung]=('0');
			}*/
			System.out.println(groesser[nullPruefung]);
		} 
		
		for(int i =laenge; i>-1; i--){
			if(laenger1){
				ergebnis = ergebnis + ((Integer.parseInt(""+groesser[i])+ Integer.parseInt(""+summand1[i]))*zehnerpotenz);
			}else if(laenger2){
				ergebnis = ergebnis + ((Integer.parseInt(""+summand2[i])+ Integer.parseInt(""+groesser[i]))*zehnerpotenz);
			}else {
			ergebnis = ergebnis + ((Integer.parseInt(""+summand2[i])+ Integer.parseInt(""+summand1[i]))*zehnerpotenz);
			}
			zehnerpotenz = zehnerpotenz *10;
					
		}
		
		System.out.println(ergebnis);
		return ergebnis;	
	}
	
	
	public static void main(String[] args) {
		Aufgabe1 aufgabe1 = new Aufgabe1();
		aufgabe1.Addieren(255, 25);
	}

}
