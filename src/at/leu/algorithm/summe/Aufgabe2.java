package at.leu.algorithm.summe;

public class Aufgabe2 {
	
	public double Multiplizieren(int a, int b) {
	    String k = new StringBuilder("" + a).toString();
	    String p = new StringBuilder("" + b).toString();
	    char[] zahl1 = k.toCharArray();
	    char[] zahl2 = p.toCharArray();
	    int kleiner;
	    int laenge = 0;
	    char[] groesser;
	    if (a < b) {
	      laenge = zahl2.length;
	      groesser = zahl2;
	      kleiner = a;
	    } else {
	      laenge = zahl1.length;
	      groesser = zahl1;
	      kleiner = b;
	    }
	    int zehnerpotenz = (int) Math.pow(10, laenge - 1);
	    int res = 0;
	    for (int i = 0; i < laenge; i++) {
	      int temp = Integer.parseInt("" + groesser[i]) * kleiner * zehnerpotenz;
	      res += temp;
	      zehnerpotenz /= 10;

	    }
	    System.out.println(res);
	    return res;
	  }
	
	public static void main(String[] args) {
		Aufgabe2 aufgabe2 = new Aufgabe2();
		aufgabe2.Multiplizieren(200, 25);
	}


}
