package at.leu.algorithm;

import at.leu.algorithm.exception.NumberNotFoundException;
import at.leu.algorithm.helper.DataGenerator;
import at.leu.algorithm.searchalgorithms.BinarySearch;
import at.leu.algorithm.searchalgorithms.SequentialSearch;
import at.leu.algorithm.sortalgorithms.BubbleSorter;
import at.leu.algorithm.sortalgorithms.InsertionSorter;
import at.leu.algorithm.sortalgorithms.SortAlgorithm;

public class Main {
	public static void main(String[] args) {
		
	
		SortAlgorithm s = new InsertionSorter(); 
		int[] randomData = DataGenerator.generateRandomData(666666666, 1, 560);
		SortEngine se = new SortEngine();
		se.setAlgorithm(s);
		//DataGenerator.printData(se.sort(randomData));
		BinarySearch bs = new BinarySearch();
		try {
			System.out.println("Index: "+bs.searchBinary(301, randomData));
		} catch (NumberNotFoundException e) {
			e.printStackTrace();
		}
		
		
		/*int[] randomData2 = DataGenerator.generateRandomData(25, 1,50);
		SequentialSearch sS = new SequentialSearch();
		try {
			System.out.println(sS.searchInt(500,randomData2));
		} catch (NumberNotFoundException e) {
			e.printStackTrace();
		}*/
		
		
		
		
		
	}
}
