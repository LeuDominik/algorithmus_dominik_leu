package at.leu.algorithm.test.sorting;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BubbleSorterTest.class, SelectionSorterTest.class })
public class AllTests {

}
