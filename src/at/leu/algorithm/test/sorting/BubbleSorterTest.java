package at.leu.algorithm.test.sorting;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import at.leu.algorithm.sortalgorithms.BubbleSorter;


public class BubbleSorterTest {

	private int[]data = new int[3];

	@Before
	public void setUp() throws Exception{
		data[0]=45;
		data[1]=9;
		data[2]=26;
	}
	
	@Test
	public void test() {
		int[] unsorted = data.clone();
		BubbleSorter ss = new BubbleSorter();
		int[] res = ss.sort(unsorted);
		
		assertTrue(isSorted(res));
	}
	
	private boolean isSorted(int[]sorted){
		int old = sorted[0];
		for(int i = 1; i< sorted.length; i++){
			if (old< sorted[i]){
				old = sorted[i];
			}else{
				return false;
			}
		}
		return true;
	}

}
