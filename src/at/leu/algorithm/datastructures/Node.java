package at.leu.algorithm.datastructures;

public class Node {
	private int value;
	private Node next;
	private Node prev;
	
	public Node(){}
	public Node(int Value){
		value = Value;
	}

	public Node getNext() {
		return next;
	}

	public void setNext(Node next) {
		this.next = next;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public Node getPrev() {
		return prev;
	}
	public void setPrev(Node prev) {
		this.prev = prev;
	}
	
	

}
