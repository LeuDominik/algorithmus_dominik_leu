package at.leu.algorithm.datastructures;

public class List {
	private Node root;
	
	public List(){}
	
	public Node next(Node Current){
		Node next = new Node();
		
		if(Current.getNext()==null){
			next = null;
		}else{
			next = Current.getNext();
		}
		
		return next;
	}

	public Node previous(Node Current){
		Node previous = new Node();
		
		if(Current.getPrev()==null){
			previous =null;
		}else{ 
			previous = Current.getPrev();
		}
		
		return previous;
	}
	
	public Node get(int position){
		Node current = root;
		for(int count =0; count<position-1; count ++){
			current = current.getNext();
		}
		return  current;
	}
	
	
	public void add(int value){
		
		Node current = new Node();
		Node newNode = new Node(value);
		
		if(root == null){
			root = new Node(value);
		}else{
			current = root;
			while(current.getNext()!=null){
				current = current.getNext();
			}
			current.setNext(newNode);
			newNode.setPrev(current);
		}
	}
	
	public void addAtPosition(int value, int position){
		Node current = new Node();
		Node newNode = new Node(value);
		
		
		
		
	}

}
