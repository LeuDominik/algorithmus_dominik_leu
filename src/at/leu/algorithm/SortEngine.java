package at.leu.algorithm;

import at.leu.algorithm.sortalgorithms.SortAlgorithm;

public class SortEngine {
	private SortAlgorithm sortAgorithm;
	
	public int[] sort(int[] data){
		return sortAgorithm.sort(data);
	}
	
	public void setAlgorithm(SortAlgorithm algo){
		this.sortAgorithm =algo; 
	}

}
