package at.leu.algorithm.searchalgorithms;

import at.leu.algorithm.exception.NumberNotFoundException;
import at.leu.algorithm.helper.DataGenerator;

public class SequentialSearch {
	
	public int searchInt(int input, int[] random) throws NumberNotFoundException{
		int result =-1;
		int size = random.length;
		int position = 0;
		
		Boolean found = false;
		
		for(int i=0; i <size; i++){
			if(random[i]==input){
				result =i;
				position = i;
			
				found = true;
				System.out.println("Die Sucheingabe " +input+" wurde an "+position+" Position gefunden!");
			}
		}
		
		if(!found){
			throw new NumberNotFoundException("Nicht gefunden!!!!");
		}
		return result;
	}

}
