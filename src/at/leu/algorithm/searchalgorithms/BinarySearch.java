package at.leu.algorithm.searchalgorithms;

import at.leu.algorithm.exception.NumberNotFoundException;

public class BinarySearch {
	
	public int searchBinary(int input, int[]randomList) throws NumberNotFoundException{
		int size = randomList.length-1;
		int binary = size/2;
		Boolean found = false;
		int differenzHigh = 0;
		 
		
		while(!found&&binary>1){
			differenzHigh = size-binary;
			
			if(randomList[binary]>input){
				binary = binary/2;
			}else if(randomList[binary]<input){
				binary = binary + (differenzHigh/2);
			}else{
				found = true;
			}		
		}
		if(!found){
			throw new NumberNotFoundException("Nicht gefunden!!!!");
		}
		System.out.println(randomList[binary]);
		return binary;
	}

}
