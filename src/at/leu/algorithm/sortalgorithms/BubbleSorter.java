package at.leu.algorithm.sortalgorithms;

public class BubbleSorter implements SortAlgorithm{

	@Override
	public int[] sort(int[] unsortedData) {
		int size = unsortedData.length;
		int counter = 0;
		int temp;
		
		
		for(int rounds = 0; rounds< size; rounds++){
			counter = 0;
			for(int try1 = 0; try1< size; try1++){
				if((counter +1 != size) &&(unsortedData[counter]> unsortedData[counter+1])){
					temp = unsortedData[counter];
					unsortedData[counter]= unsortedData[counter +1 ];
					unsortedData[counter +1]=temp;
				}
				counter ++; 
			}
		}
		
		return unsortedData;
	}

	@Override
	public String getName() {
		
		return "BubbleSorter";
	}
}
