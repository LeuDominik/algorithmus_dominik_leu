package at.leu.algorithm.sortalgorithms;

public interface SortAlgorithm {
	public int[] sort(int[] data);
	public String getName();

}
