package at.leu.algorithm.sortalgorithms;

import at.leu.algorithm.helper.DataGenerator;

public class SelectionSorter implements SortAlgorithm{

	

	@Override
	public int[] sort(int[] unsortedData) {
		int size = unsortedData.length;
		int highest = 0;
		int positionHighest=0;
		int temp;
		
		for(int rounds = size; rounds > 0; rounds --){
			
			for(int position = 0; position < rounds; position ++){
				if(unsortedData[position]>highest){
					highest = unsortedData[position];
					positionHighest = position;
				}
			}
			temp = unsortedData[positionHighest];
			unsortedData[positionHighest] = unsortedData[rounds-1];
			unsortedData[rounds-1]= temp;
			
			
			highest =0;
			positionHighest = 0;
		}
		
		
		return unsortedData;
	}

	@Override
	public String getName() {
		
		return "SelectionSorter";
	}
	
}
