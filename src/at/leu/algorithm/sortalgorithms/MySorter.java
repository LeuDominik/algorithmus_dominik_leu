package at.leu.algorithm.sortalgorithms;

public class MySorter implements SortAlgorithm{

	@Override
	public int[] sort(int[] unsortedData) {
		int size = unsortedData.length;
		int[] sortedData = new int[size];
		int sorted = 0;
		int temp;

		while(sorted!=size){
			int counter = 0;
			
			
			sortedData[sorted]= unsortedData[sorted];
			
			if(sorted == 0){
				sorted++;
			}else{
				while(sorted-counter-1 != -1 && sortedData[sorted-counter] < sortedData[sorted-counter -1]){
					temp = sortedData[sorted-counter];
					sortedData[sorted-counter]= sortedData[sorted-counter -1 ];
					sortedData[sorted-counter -1]=temp;

					counter =counter +1;
					
				}
				sorted ++; 
				
			}
		}
		
		return sortedData;
	}
	
	@Override
	public String getName(){
		
		return "MySorter";
	}

}
