package at.leu.algorithm.sortalgorithms;

public class InsertionSorter implements SortAlgorithm{

	@Override
	public int[] sort(int[] unsortedData) {
		int size = unsortedData.length;
		int test = 0;
		int temp = 0;
		
		for(int correct =0; correct< size; correct++){
			while(correct - test != -1&& correct +1 !=size && unsortedData[correct-test] > unsortedData[correct+1-test]){
				temp = unsortedData[correct-test];
				unsortedData[correct-test] = unsortedData[correct-test+1];
				unsortedData[correct-test+1]= temp;
				
				test++;
			}
			test =0;
		}
		
		return unsortedData;
	}

	@Override
	public String getName() {
		
		return "InsertionSorter";
	}

}
